create schema inventory collate utf8_general_ci;

create table categories
(
	id int auto_increment,
	title varchar(255) not null,
	description text null,
	constraint categories_pk
		primary key (id)
);

create table locations
(
	id int auto_increment,
	title varchar(255) not null,
	description text null,
	constraint locations_pk
		primary key (id)
);

create table items
(
	id int auto_increment,
	category_id int null,
	location_id int null,
	title varchar(255) not null,
	description text null,
	image varchar(255) null,
	constraint items_pk
		primary key (id),
	constraint items_categories_id_fk
		foreign key (category_id) references categories (id)
			on update cascade,
	constraint items_locations_id_fk
		foreign key (location_id) references locations (id)
			on update cascade
);




