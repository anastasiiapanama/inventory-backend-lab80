const express = require('express');
const path = require('path');
const multer = require('multer');

const mysqlDb = require('../mysqlDb');

const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {

    const [items] = await mysqlDb.getConnection().query('SELECT * FROM items');

    res.send(items);
});

router.get('/:id', async (req, res) => {
    const [items] = await mysqlDb.getConnection().query('SELECT * FROM items WHERE id = ?', [req.params.id]);

    const item = items[0];

    res.send(item);
});

router.post('/', upload.single('image'), async (req, res) => {
    const item = req.body;

    if (req.file) {
        item.image = req.file.filename;
    }

    const [result] = await mysqlDb.getConnection().query('INSERT INTO items (category_id, location_id, title, ' +
        'description, image) VALUES (?, ?, ?, ?, ?)',
        [item.category_id, item.location_id, item.title, item.description, item.image]);

    res.send({...item, id: result.insertId});
});

router.delete('/:id', async (req, res) => {
    const item = req.body;

    const [result] = await mysqlDb.getConnection().query('DELETE FROM items WHERE id = ?',
        [item.category_id, item.location_id, item.title, item.description, item.image]);

    res.send({...item, id: result.insertId});
});

router.put('/:id', async (req, res) => {
    const itemId = req.params.id;
    const item = req.body;

    if (req.file) {
        item.image = req.file.filename;
    }

    const [result] = await mysqlDb.getConnection().query(
        'UPDATE items SET category_id=?, location_id=?, title=?, description=?, image=? WHERE id = ?',
        [item.category_id, item.location_id, item.title, item.description, item.image, itemId]
    );

    res.send({...item, id: result.insertId});
});

module.exports = router;