const express = require('express');

const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
    const [categories] = await mysqlDb.getConnection().query('SELECT * FROM categories');

    res.send(categories);
});

router.get('/:id', async (req, res) => {
    const [categories] = await mysqlDb.getConnection().query('SELECT * FROM categories WHERE id = ?', [req.params.id]);

    const category = categories[0];

    res.send(category);
});

router.post('/', async (req, res) => {
    const category = req.body;

    const [result] = await mysqlDb.getConnection().query('INSERT INTO categories (title, description) VALUES (?, ?)',
        [category.title, category.description]);

    res.send({...category, id: result.insertId});
});

router.delete('/:id', async (req, res) => {
    const category = req.body;

    const [result] = await mysqlDb.getConnection().query('DELETE FROM categories WHERE id = ?',
        [category.title, category.description]);

    res.send({...category, id: result.insertId});
});

router.put('/:id', async (req, res) => {
    const itemId = req.params.id;
    const category = req.body;

    const [result] = await mysqlDb.getConnection().query(
        'UPDATE categories SET title=?, description=? WHERE id = ?',
        [category.title, category.description, itemId]
    );

    res.send({...category, id: result.insertId});
});

module.exports = router;