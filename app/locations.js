const express = require('express');

const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
    const [locations] = await mysqlDb.getConnection().query('SELECT * FROM locations');

    res.send(locations);
});

router.get('/:id', async (req, res) => {
    const [locations] = await mysqlDb.getConnection().query('SELECT * FROM locations WHERE id = ?', [req.params.id]);

    const location = locations[0];

    res.send(location);
});

router.post('/', async (req, res) => {
    const location = req.body;

    const [result] = await mysqlDb.getConnection().query('INSERT INTO locations (title, description) VALUES (?, ?)',
        [location.title, location.description]);

    res.send({...location, id: result.insertId});
});

router.delete('/:id', async (req, res) => {
    const location = req.body;

    const [result] = await mysqlDb.getConnection().query('DELETE FROM locations WHERE id = ?',
        [location.title, location.description]);

    res.send({...location, id: result.insertId});
});

router.put('/:id', async (req, res) => {
    const itemId = req.params.id;
    const location = req.body;

    const [result] = await mysqlDb.getConnection().query(
        'UPDATE locations SET title=?, description=? WHERE id = ?',
        [location.title, location.description, itemId]
    );

    res.send({...location, id: result.insertId});
});

module.exports = router;