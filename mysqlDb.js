const mysql = require('mysql2/promise');

let connection = null;

module.exports = {
    connect: async () => {
        connection = await mysql.createConnection({
            host     : 'localhost',
            user     : 'root',
            password : 'root1234',
            database: 'inventory'
        });

        console.log('Connection to MySQL successful! id=' + connection.threadId);
    },
    getConnection: () => connection
};