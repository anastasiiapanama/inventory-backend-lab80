const express = require('express');
const cors = require('cors');

const mysqlDb = require('./mysqlDb');

const categories = require('./app/categories');
const locations = require('./app/locations');
const items = require('./app/items');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/categories', categories);
app.use('/locations', locations);
app.use('/items', items);

const run = async () => {
    await mysqlDb.connect();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};

run().catch(console.error);